package io.geekhub.tests;

public interface OperationWithTwoArguments extends Operation {

   Number calculate(Number x, Number y);

}
