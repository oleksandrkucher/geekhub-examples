package io.geekhub.tests;

import java.util.Objects;

import static java.util.Objects.isNull;

public class Calculator {
    int add(int ... args){
        int sum = 0;
        for (int arg : args) {
           sum += arg;
        }
        return sum;
    }

    double sqrt(Double x) {
        if(isNull(x) || x < 0){
            throw new IllegalArgumentException("Can not calculate square root for not positive number");
        }

        return Math.sqrt(x);
    }
}
