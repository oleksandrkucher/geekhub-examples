package io.geekhub.tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;


public class CalculatorTest {

    private Calculator calculator;

    @BeforeMethod
    public void setUp() throws Exception {
        calculator = new Calculator();
    }

    @DataProvider(name = "correctNubers")
    public static Object[][] correctNubers() {
        return new Object[][]{
                {new int[]{12,3}, 15, "asd"},
                {new int[]{}, 0, "asdas"},
                {new int[]{-10, 10, 10}, 10, "asdasd"}
        };
    }

    @Test(dataProvider = "correctNubers")
    public void testAdd(int[] args, int expcdSum, String s) {
        int sum = calculator.add(args);
        assertEquals(sum, expcdSum);
    }

    @Test
    public void testRoot4() {
        double root = calculator.sqrt(4.0);
        assertEquals(root, 2.0);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testRootNegative() {
        double root = calculator.sqrt(-4.0);

    }

    @Test
    public void testRootZero() {
        double root = calculator.sqrt(0.0);
        assertEquals(root, 0.0);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testRootNull() {
        calculator.sqrt(null);
    }
}