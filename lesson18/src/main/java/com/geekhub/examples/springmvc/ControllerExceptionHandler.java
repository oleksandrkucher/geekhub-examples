package com.geekhub.examples.springmvc;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import java.io.FileNotFoundException;

@ControllerAdvice(
        basePackages = {"com.geekhub.examples.springmvc"},
        assignableTypes = {HomeController.class}
)
public class ControllerExceptionHandler {
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler({FileNotFoundException.class, RuntimeException.class})
    public ModelAndView exceptionHandler(Exception ex) {
        ModelAndView mav = new ModelAndView("serviceException");
        mav.addObject("errorMessage", "Some Message. " + ex.toString());
        return mav;
    }
}