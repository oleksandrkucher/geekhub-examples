package com.geekhub.examples.templatemethod;

import com.geekhub.examples.templatemethod.templates.PassExamTemplate;

public class Student {
    private PassExamTemplate template;

    public Student(PassExamTemplate template) {
        this.template = template;
    }

    public PassExamTemplate getTemplate() {
        return template;
    }
}
