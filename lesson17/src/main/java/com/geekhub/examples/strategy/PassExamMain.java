package com.geekhub.examples.strategy;

import com.geekhub.examples.Exam;
import com.geekhub.examples.strategy.strategies.BadStudentPassExamStrategy;
import com.geekhub.examples.strategy.strategies.ExcellentStudentPassExamStrategy;
import com.geekhub.examples.strategy.strategies.RegularStudentPassExamStrategy;

import java.time.LocalDateTime;
import java.time.Month;

public class PassExamMain {
    public static void main(String[] args) {
        final Exam exam = new Exam();
        exam.setName("Programing Languages");
        exam.setTeacher("Serdyuk O. A.");
        exam.setTime(LocalDateTime.of(2018, Month.MAY, 25, 9, 0));

        passExam(new Student(new ExcellentStudentPassExamStrategy()), exam);
        passExam(new Student(new BadStudentPassExamStrategy()), exam);
        passExam(new Student(new RegularStudentPassExamStrategy()), exam);
    }

    private static void passExam(Student student, Exam exam) {
        student.getStrategy().passExam(exam);
    }
}
