package com.geekhub.examples.dependencyinjection.application;

import com.geekhub.examples.dependencyinjection.dependencyinjector.DependencyInjector;
import com.geekhub.examples.dependencyinjection.dependencyinjector.DependencyInjectorImpl;

public class Application {
    public static void main(String[] args) {
        final DependencyInjector context = new DependencyInjectorImpl(Application.class);
        context.getBean(Worker.class).printPrimeNumbers(10);
        context.close();
    }
}
