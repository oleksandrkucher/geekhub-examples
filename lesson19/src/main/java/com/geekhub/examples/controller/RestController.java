package com.geekhub.examples.controller;

import com.geekhub.examples.dto.UserDto;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.SessionAttribute;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;

@org.springframework.web.bind.annotation.RestController
public class RestController {
    @GetMapping("rest")
    public List<String> rest() {
        return Arrays.asList("str1", "str2", "str3", "qwerty");
    }

    @GetMapping("user-from-form")
    @ResponseStatus(HttpStatus.OK)
    public UserDto modelAttribute(@ModelAttribute UserDto user) {
        return user;
    }

    @PostMapping("user-from-body")
    public UserDto requestBody(@RequestBody UserDto user) {
        return user;
    }

    @GetMapping("put-to-session")
    public void toSession(@RequestParam String data, HttpServletRequest request) {
        request.getSession().setAttribute("data", data);
    }

    @GetMapping("get-from-session")
    public String fromSession(@SessionAttribute String data) {
        return data;
    }
}
