package com.geekhub.examples.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
    @Value("${property.a}") private String prop;

    @GetMapping(value = "/")
    public String home(Model model) {
        model.addAttribute("prop", prop);
        return "home";
    }

    @GetMapping(value = "exception")
    public String exception() {
        if (true) {
            throw new RuntimeException("AAAAA");
        }
        return "home";
    }
}
