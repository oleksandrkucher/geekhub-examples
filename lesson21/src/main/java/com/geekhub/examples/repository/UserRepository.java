package com.geekhub.examples.repository;

import com.geekhub.examples.dtos.User;

public interface UserRepository {
    User findBy(Integer id);
}
