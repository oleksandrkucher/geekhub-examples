<%@ page import="java.util.Date" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="date" value="<%=new Date()%>"/>
Today is <fmt:formatDate value="${date}" pattern="YYYY-MM-dd HH:mm:ss"/>
