package com.geekhub.examples

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

class MySecondTask extends DefaultTask {
    MySecondTask() {
        description = "Gradle task on Groovy. Task makes testString.toUpperCase()."
    }

    String testString

    @TaskAction
    void action() {
        println "Task: $name, description: $description"
        println 'This is action of my first gradle task.'
        if (null != testString) {
            println "Original string: $testString, uppercase string: ${testString.toUpperCase()}"
        } else {
            println "You didn't use customization for testString."
        }
    }
}
