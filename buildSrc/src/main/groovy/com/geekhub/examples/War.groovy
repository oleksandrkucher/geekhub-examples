package com.geekhub.examples

import org.gradle.api.Transformer

class War extends org.gradle.api.tasks.bundling.War {
    War() {
        super()

        filesMatching("css/**/*.css") {
            filter(new Transformer<String, String>() {
                @Override
                String transform(String input) {
                    return new CSSMinimizer().minimize(input)
                }
            })
        }


//        filesMatching("css/**/*.css", new Action<FileCopyDetails>() {
//            @Override
//            void execute(FileCopyDetails fileCopyDetails) {
//                filter(new Transformer<String, String>() {
//                    @Override
//                    String transform(String input) {
//                        println fileCopyDetails.path
//                        def output = new CSSMinimizer().minimize(input)
//                        println(output)
//                        return output
//                    }
//                })
//            }
//        })
    }
}