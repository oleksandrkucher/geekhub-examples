package com.geekhub.examples;

import com.yahoo.platform.yui.compressor.CssCompressor;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

public class CSSMinimizer {
    public String minimize(String content) {
        try (final StringReader reader = new StringReader(content);
             final StringWriter result = new StringWriter()) {
            final CssCompressor compressor = new CssCompressor(reader);
            compressor.compress(result, -1);
            return result.toString();
        } catch (IOException e) {
            return null;
        }
    }
}
