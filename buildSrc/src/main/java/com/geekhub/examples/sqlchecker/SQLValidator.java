package com.geekhub.examples.sqlchecker;

import groovy.lang.Tuple2;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class SQLValidator {
    private final String url;
    private final String username;
    private final String password;

    public SQLValidator(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    public List<Tuple2<SQLData, List<String>>> validateAll(List<SQLData> queries) {
        return queries
                .stream()
                .map(this::validate)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public Tuple2<SQLData, List<String>> validate(SQLData data) {
        //use url, username, password and data.getQuery()
        // to connect to database and validate SQL saved into database
        if ("ERROR_TABLE_NAME".equals(data.getTable())) {
            return new Tuple2<>(data, Collections.singletonList("BROKEN SQL QUERY"));
        } else {
            return null;
        }
    }
}
