package com.geekhub.examples.sqlchecker;

public class SQLData {
    private String table;
    private String query;

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

}
