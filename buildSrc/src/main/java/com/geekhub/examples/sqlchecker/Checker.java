package com.geekhub.examples.sqlchecker;

import groovy.lang.Tuple2;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.Task;

import java.util.List;

public class Checker implements Plugin<Project> {
    @Override
    public void apply(Project project) {
        final Task validateTask = project.getTasks().create("validateSQL");
        final CheckerExtension data = project.getExtensions().create("validateSQL", CheckerExtension.class);
        validateTask.doLast(task -> {
            final SQLValidator validator = new SQLValidator(data.getUrl(), data.getUsername(), data.getPassword());
            final List<Tuple2<SQLData, List<String>>> errors = validator.validateAll(data.getData());

            if (!errors.isEmpty()) {
                project.getLogger().error("SQL queries validation failed:");
                for (Tuple2<SQLData, List<String>> error : errors) {
                    project.getLogger().error(
                            error.getFirst().getTable() + ":\n    "
                            + String.join("\n    ", error.getSecond())
                    );
                }
                throw new RuntimeException("SQL queries validation failed.");
            }
        });
    }
}
