package io.testsgh.contexttest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@ContextConfiguration(classes = {SimpleContextConfig.class})
@WebAppConfiguration
public class UserControllerTest extends AbstractTestNGSpringContextTests {

    @Autowired private UserController userController;
    @Autowired private UserRepository userRepository;

    private MockMvc mockMvc;

    @BeforeMethod
    public void setUp() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(userController)
                .build();
    }

    @AfterMethod
    public void tearDown() {
        userRepository.deleteAll();
    }

    @Test
    public void testSave() throws Exception {
        mockMvc.perform(
                post("/users/save")
                        .param("login", "batman")
                        .param("password", "bruce_wayne")
        ).andExpect(status().isOk());
    }
}