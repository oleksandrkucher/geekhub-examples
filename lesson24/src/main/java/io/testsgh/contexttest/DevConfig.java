package io.testsgh.contexttest;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("DEV")
public class DevConfig {
    @Bean
    public ProfileComponent profileComponent() {
        return () -> "DEV_PROFILE";
    }
}
