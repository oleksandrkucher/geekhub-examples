package io.testsgh.contexttest;

import java.util.HashMap;
import java.util.Map;

public class UserRepositoryImpl {
    private final Map<String, String> storage;

    public UserRepositoryImpl() {
        storage = new HashMap<>();
    }

    public String repositoryType() {
        return "inmemory";
    }

    public void deleteAll() {
        storage.clear();
    }

    public void save(String login, String password) {
        storage.put(login, password);
    }

    public boolean contains(String login) {
        return storage.containsKey(login);
    }

    public String findOneBy(String login) {
        return storage.get(login);
    }
}
