package io.testsgh.contexttest;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("TEST")
public class TestConfig {
    @Bean
    public ProfileComponent profileComponent() {
        return () -> "TEST_PROFILE";
    }
}
