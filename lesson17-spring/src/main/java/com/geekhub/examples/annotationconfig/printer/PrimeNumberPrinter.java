package com.geekhub.examples.annotationconfig.printer;

public interface PrimeNumberPrinter {
    void printPrimeNumbers(int count);
}
