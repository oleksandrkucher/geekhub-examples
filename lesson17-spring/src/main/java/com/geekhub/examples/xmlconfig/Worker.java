package com.geekhub.examples.xmlconfig;

import com.geekhub.examples.xmlconfig.printer.PrimeNumberPrinter;

public class Worker {
    private PrimeNumberPrinter primeNumberPrinter;

    public Worker(PrimeNumberPrinter primeNumberPrinter) {
        this.primeNumberPrinter = primeNumberPrinter;
    }

    public void printPrimeNumbers(int number) {
        primeNumberPrinter.printPrimeNumbers(number);
    }
}
