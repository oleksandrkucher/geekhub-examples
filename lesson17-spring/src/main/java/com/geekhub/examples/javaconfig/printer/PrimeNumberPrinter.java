package com.geekhub.examples.javaconfig.printer;

public interface PrimeNumberPrinter {
    void printPrimeNumbers(int count);
}
