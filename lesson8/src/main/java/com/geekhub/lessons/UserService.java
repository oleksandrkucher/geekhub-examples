package com.geekhub.lessons;

import java.util.List;

public interface UserService {
    void saveUser(User user);

    User findByLogin(String login);

    List<User> findAll();

    List<User> findAllOrderByLicenseExpirationDate();

    List<User> findAllOrderByFullNameAndLogin();
}
