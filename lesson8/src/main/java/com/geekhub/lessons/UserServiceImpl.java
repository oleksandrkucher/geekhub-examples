package com.geekhub.lessons;

import java.util.List;

public class UserServiceImpl implements UserService {

    private final UserSource userSource;
    private final UserValidator userValidator;

    public UserServiceImpl(UserSource userSource, UserValidator userValidator) {
        this.userSource = userSource;
        this.userValidator = userValidator;
    }

    @Override
    public void saveUser(User user) {
        userValidator.validateUser(user);
        userSource.addUser(user);
    }

    @Override
    public User findByLogin(String login) {
        return null;
    }

    @Override
    public List<User> findAll() {
        return null;
    }

    @Override
    public List<User> findAllOrderByLicenseExpirationDate() {
        return null;
    }

    @Override
    public List<User> findAllOrderByFullNameAndLogin() {
        return null;
    }
}
