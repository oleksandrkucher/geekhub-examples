package com.geekhub.lessons;

import com.geekhub.lessons.factories.DrivableFactory;
import com.geekhub.lessons.factories.SUVFactoryImpl;
import com.geekhub.lessons.factories.SedanFactoryImpl;
import com.geekhub.lessons.interfaces.Drivable;

public class Demo {
    public static void main(String[] args) {
        ControlPanel panel = new ControlPanel();
        for (DrivableFactory factory : factories()) {
            Drivable drivable = factory.create();
            System.out.println("Test Drive: " + drivable.getName());
            panel.switchOn(drivable);
            System.out.println(drivable.getEngine().getStatus());
            panel.accelerate(drivable);
            System.out.println(drivable.getAccelerator().getStatus());
            panel.slowDown(drivable);
            panel.switchOff(drivable);
            System.out.println("Test Drive Successfully Finished.");
        }
    }

    private static DrivableFactory[] factories() {
        return new DrivableFactory[] {
                new SedanFactoryImpl(),
                new SUVFactoryImpl()
        };
    }
}
