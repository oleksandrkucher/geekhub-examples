package com.geekhub.lessons.parts;

import com.geekhub.lessons.interfaces.StatusAware;

public class Engine implements StatusAware {
    private boolean status;
    private final int power;
    private final EngineType type;

    public Engine(int power, EngineType type) {
        this.power = power;
        this.type = type;
    }

    public void switchOn() {
        status = true;
    }

    public void switchOff() {
        status = false;
    }

    public int getPower() {
        return power;
    }

    public EngineType getType() {
        return type;
    }

    @Override
    public String toString() {
        return getStatus();
    }

    @Override
    public String getStatus() {
        return String.valueOf(status);
    }
}
