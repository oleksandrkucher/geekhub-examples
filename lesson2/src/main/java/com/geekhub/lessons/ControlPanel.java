package com.geekhub.lessons;

import com.geekhub.lessons.interfaces.Drivable;

public class ControlPanel {
    public void switchOn(Drivable drivable) {
        drivable.getEngine().switchOn();
    }

    public void switchOff(Drivable drivable) {
        drivable.getEngine().switchOff();
    }

    public void accelerate(Drivable drivable) {
        drivable.getAccelerator().accelerate();
    }

    public void slowDown(Drivable drivable) {
        System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAA");
    }
}
