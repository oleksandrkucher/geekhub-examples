package com.geekhub.examples.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class IndexController {
    private final LibraryManager libraryManager;

    public IndexController(LibraryManager libraryManager) {
        this.libraryManager = libraryManager;
    }

    @RequestMapping("/")
    public String index(@RequestParam long isbn,  Model model) {
        model.addAttribute("book", libraryManager.getBook(isbn));
        return "index";
    }

    @RequestMapping("add")
    @ResponseBody
    public void add(@RequestParam String name, @RequestParam String author) {
        libraryManager.putBook(name, author);
    }
}
