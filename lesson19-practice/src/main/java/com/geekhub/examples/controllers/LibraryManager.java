package com.geekhub.examples.controllers;

import com.geekhub.examples.data.dto.Book;
import com.geekhub.examples.services.bookmanager.BookManager;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@Component
public class LibraryManager {
    private final AtomicLong isbnGen;
    private final List<BookManager> bookManagers;

    public LibraryManager(List<BookManager> bookManagers) {
        this.bookManagers = bookManagers;
        isbnGen = new AtomicLong(1);
    }

    public void putBook(String name, String author) {
        Book book = new Book(isbnGen.getAndIncrement(), name, author);
        bookManagers.forEach(bm -> bm.putBook(book));
    }

    public Book getBook(long isbn) {
        return bookManagers.parallelStream().map(bm -> bm.getBook(isbn)).findAny().orElse(null);
    }
}
