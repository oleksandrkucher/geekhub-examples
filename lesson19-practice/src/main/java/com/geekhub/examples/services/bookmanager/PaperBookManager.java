package com.geekhub.examples.services.bookmanager;

import com.geekhub.examples.data.dto.Book;
import com.geekhub.examples.services.bookcontainer.BookContainer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class PaperBookManager implements BookManager {
    private final BookContainer bookContainer;

    public PaperBookManager(@Qualifier("поличка") BookContainer bookContainer) {
        this.bookContainer = bookContainer;
    }

    @Override
    public void putBook(Book book) {
        bookContainer.putBook(book);
    }

    @Override
    public Book getBook(long isbn) {
        return bookContainer.getBook(isbn);
    }
}
