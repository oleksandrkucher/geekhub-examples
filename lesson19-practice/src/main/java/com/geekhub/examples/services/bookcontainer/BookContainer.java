package com.geekhub.examples.services.bookcontainer;

import com.geekhub.examples.data.dto.Book;

public interface BookContainer {
    void putBook(Book book);

    Book getBook(long isbn);
}
