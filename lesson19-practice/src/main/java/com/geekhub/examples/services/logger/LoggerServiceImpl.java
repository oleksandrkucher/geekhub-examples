package com.geekhub.examples.services.logger;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public class LoggerServiceImpl implements LoggerService {
    @Override
    public void log(Object object) {
        System.out.println(object);
        try {
            Files.write(Paths.get("d:\\file.txt"), String.valueOf(object).getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
