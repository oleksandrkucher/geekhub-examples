package com.geekhub.examples.services.bookcontainer;

import com.geekhub.examples.data.dto.Book;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component(value = "поличка")
public class BookShelve implements BookContainer {
    private final Map<Long, Book> container;

    public BookShelve() {
        container = new HashMap<>();
    }

    @Override
    public void putBook(Book book) {
        container.put(book.getIsbn(), book);
    }

    @Override
    public Book getBook(long isbn) {
        return container.get(isbn);
    }
}
