package com.geekhub.examples.services.bookcontainer;

import com.geekhub.examples.data.dto.Book;
import com.geekhub.examples.services.logger.LoggerService;
import org.springframework.stereotype.Component;

@Component
public class EBookStorage implements BookContainer {
    private final LoggerService loggerService;

    public EBookStorage(LoggerService loggerService) {
        this.loggerService = loggerService;
    }

    @Override
    public void putBook(Book book) {
        loggerService.log("write to memory");
    }

    @Override
    public Book getBook(long isbn) {
        loggerService.log("read from memory");
        return null;
    }
}
