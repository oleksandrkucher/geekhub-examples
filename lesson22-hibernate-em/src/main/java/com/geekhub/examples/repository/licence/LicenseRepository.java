package com.geekhub.examples.repository.licence;

import com.geekhub.examples.db.persistence.License;
import com.geekhub.examples.repository.GeneralRepository;

public interface LicenseRepository extends GeneralRepository<License, Integer> {
}
