package com.geekhub.examples.repository.licence;

import com.geekhub.examples.db.persistence.License;
import com.geekhub.examples.repository.GeneralRepositoryImpl;
import org.springframework.stereotype.Repository;

@Repository
public class LicenseRepositoryImpl extends GeneralRepositoryImpl<License, Integer> implements LicenseRepository {
    public LicenseRepositoryImpl() {
        super(License.class);
    }
}
