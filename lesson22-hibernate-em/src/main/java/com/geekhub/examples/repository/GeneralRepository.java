package com.geekhub.examples.repository;

import com.geekhub.examples.db.persistence.Persistable;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

public interface GeneralRepository<T extends Persistable<PK>, PK extends Serializable> {
    Optional<T> findBy(PK id);

    List<T> findAll();

    T save(T user);

    void delete(T user);
}
