package com.geekhub.examples;

import java.util.function.Predicate;

import static java.util.stream.Collectors.reducing;
import static java.util.stream.Collectors.summarizingDouble;

public class Main {

    public static final String NEW_LINE = "\n";

    public static void main(String[] args) {
        String testText = ResourceUtils.readResource("test_std.txt");

        final Predicate<String> answerCorrectnessChecker = id -> {
            try {
                Integer.valueOf(id);
                return true;
            } catch (NumberFormatException e){
                return false;
            }
        };
        final Predicate<String> commentLineDetector = line -> line.startsWith("//");
        final String linesDelimiter = "\\R";


        TestParser testParser = new TestParser(
                linesDelimiter,
                answerCorrectnessChecker,
                commentLineDetector
        );
        Test test = testParser.parseTest(testText);
        System.out.println(test);
    }
}
