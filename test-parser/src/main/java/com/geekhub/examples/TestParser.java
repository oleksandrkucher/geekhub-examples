package com.geekhub.examples;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class TestParser {
    private final String linesDelimiter;
    private final Predicate<String> answerCorrectnessChecker;
    private final Predicate<String> commentLineDetector;

    public TestParser(String linesDelimiter, Predicate<String> answerCorrectnessChecker, Predicate<String> commentLineDetector) {
        this.linesDelimiter = linesDelimiter;
        this.answerCorrectnessChecker = answerCorrectnessChecker;
        this.commentLineDetector = commentLineDetector;
    }

    public Test parseTest(String testText) {
        String commentlessTestText = cleanUpTestText(testText);
        String[] questionsA = commentlessTestText.split(linesDelimiter + "---" + linesDelimiter);

        QuestionParser questionParser = new QuestionParser(answerCorrectnessChecker);
        List<Question> questions = questionParser.parseQuestions(questionsA);
        return new Test("GH Final test", questions);
    }

    private String cleanUpTestText(String testText) {
        String[] testLines = testText.split(linesDelimiter);
        return Arrays.stream(testLines)
                .filter(line -> !isCommentedLine(line))
                .collect(Collectors.joining("\n"));
    }

    private boolean isCommentedLine(String line) {
        return commentLineDetector.test(line);
    }
}