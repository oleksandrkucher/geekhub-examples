package com.geekhub.examples;

import java.util.List;

public class Test {
    private final String label;
    private final List<Question> questions;

    public Test(String label, List<Question> questions) {
        this.label = label;
        this.questions = questions;
    }

    @Override
    public String toString() {
        return "Test{" +
                "label='" + label + '\'' +
                ", questions=" + questions +
                '}';
    }
}
