package com.geekhub.examples;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class QuestionParserTest {


    @Test
    public void parseQuestion_ShouldParseId_CorrectInput() {
        QuestionParser questionParser = new QuestionParser(
                id -> id.equals("_")
        );

        String questionText =
                "25. Which of the following options correctly create an ExecutorService instance?\t\n"
                + "|a) ExecutorService es = ExecutorService.getInstance();\n"
                + "|b) ExecutorService es = new ExecutorService();\n"
                + "|c) ExecutorService es = Executor.getSingleThreadExecutor();\n"
                + "|_) ExecutorService es = Executors.newFixedThreadPool(2);\n";
        Question question = questionParser.parseQuestion(questionText, id -> id.startsWith("_"));
        Assert.assertEquals(25, question.getId());
    }

    @Test
    public void parseQuestion_ShouldParseText_CorrectInput() {
        QuestionParser questionParser = new QuestionParser(
                id -> id.equals("_")
        );

        String questionText =
                "25. Which of the following options correctly create an ExecutorService instance?\t\n"
                + "|a) ExecutorService es = ExecutorService.getInstance();\n"
                + "|b) ExecutorService es = new ExecutorService();\n"
                + "|c) ExecutorService es = Executor.getSingleThreadExecutor();\n"
                + "|_) ExecutorService es = Executors.newFixedThreadPool(2);\n";
        Question question = questionParser.parseQuestion(questionText, id -> id.startsWith("_"));
        Assert.assertEquals(
            "Which of the following options correctly create an ExecutorService instance?",
            question.getText()
        );
    }

    @Test
    public void parseQuestion_ShouldParseAnswersCount_CorrectInput() {
        QuestionParser questionParser = new QuestionParser(
                id -> id.equals("_")
        );
        String questionText =
                "25. Which of the following options correctly create an ExecutorService instance?\t\n"
                + "|a) ExecutorService es = ExecutorService.getInstance();\n"
                + "|b) ExecutorService es = new ExecutorService();\n"
                + "|c) ExecutorService es = Executor.getSingleThreadExecutor();\n"
                + "|_) ExecutorService es = Executors.newFixedThreadPool(2);\n";
        Question question = questionParser.parseQuestion(questionText, id -> id.startsWith("_"));
        Assert.assertEquals(4, question.getAnswers().size());
    }

    @Test
    public void parseQuestion_ShouldTrimAnswerText_ForCorrectInput() {
        QuestionParser questionParser = new QuestionParser(
                id -> id.equals("_")
        );
        String questionText =
                "    \n\r\t\r25. Which of the following options correctly create an ExecutorService instance?\t\n"
                + "|a) ExecutorService es = ExecutorService.getInstance();\n"
                + "|b) ExecutorService es = new ExecutorService();\n"
                + "|c) ExecutorService es = Executor.getSingleThreadExecutor();\n"
                + "|_) ExecutorService es = Executors.newFixedThreadPool(2);\n\n\t\r\r\t\n";
        questionParser.parseQuestion(questionText, id -> id.startsWith("_"));
    }
    @Test
    public void parseQuestion_EveryQuestion_ShouldHaveCorrectAnswerCamelCase() {
        QuestionParser questionParser = new QuestionParser(
                id -> id.toUpperCase().equals(id)
        );

        String questionText =
                "1. Which of the following options correctly create an ExecutorService instance?\t\n"
                + "|a) ExecutorService es = ExecutorService.getInstance();\n"
                + "|b) ExecutorService es = new ExecutorService();\n"
                + "|c) ExecutorService es = Executor.getSingleThreadExecutor();\n"
                + "|D) ExecutorService es = Executors.newFixedThreadPool(2);";
        Question question = questionParser.parseQuestion(questionText, id -> id.toUpperCase().equals(id));
        boolean hasCorrectAnswer = question.getAnswers().stream().anyMatch(Answer::isCorrect);
        Assert.assertTrue(hasCorrectAnswer);
    }

    @Test
    public void parseQuestion_EveryQuestion_ShouldHaveCorrectAnswerUnderscore() {
        QuestionParser questionParser = new QuestionParser(
                id -> id.equals("_")
        );
        String questionText =
                "1. Which of the following options correctly create an ExecutorService instance?\t\n"
                + "|a) ExecutorService es = ExecutorService.getInstance();\n"
                + "|b) ExecutorService es = new ExecutorService();\n"
                + "|c) ExecutorService es = Executor.getSingleThreadExecutor();\n"
                + "|_) ExecutorService es = Executors.newFixedThreadPool(2);";
        Question question = questionParser.parseQuestion(questionText, id -> id.equals("_"));
        boolean hasCorrectAnswer = question.getAnswers().stream().anyMatch(Answer::isCorrect);
        Assert.assertTrue(hasCorrectAnswer);
    }
}